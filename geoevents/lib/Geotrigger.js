class Geotrigger {
  constructor() {

    // Memory
    this.pulseCount = 0;
    this.head = null;
    this.head_1 = null;
    this.head_2 = null;

    // Listeners
    this.listenerTypes = ['onEnter', 'onLeave'];
    this.listeners = this.listenerTypes.reduce((map, type) => map.set(type, []), new Map());
  }

  on(type, fc) {
    if (this.listenerTypes.indexOf(type) === -1) {
      return;
    }

    this.listeners.get(type).push(fc);
  }

  _trigger(type, event) {
    if (this.listenerTypes.indexOf(type) === -1) {
      return;
    }

    this.listeners.get(type).forEach(fc => fc.call(this, event));
  }

  pulse(h, last_pos) {
    this.pulseCount++;

    const [h1, h2, h3] = [this.head, this.head_1, this.head_2];

    if (h2 instanceof Set) {
      // onEnter events
      [...h].filter(x => !h2.has(x) && h1.has(x)).forEach(x => {
        this._trigger('onEnter', {
          id: x,
          pulse: this.pulseCount,
          last_pos,
        });
      });

      // onLeave events
      [...h2].filter(x => !h.has(x) && !h1.has(x)).forEach(x => {
        this._trigger('onLeave', {
          id: x,
          pulse: this.pulseCount,
          last_pos,
        });
      });
    }

    if (h3 instanceof Set) {
      h3.clear();
    }

    // Save
    this.head = h;
    this.head_1 = h1;
    this.head_2 = h2;
  }
}

module.exports = Geotrigger;
