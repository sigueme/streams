const polyline = require('@mapbox/polyline');
const InNOut = require('in-n-out');

class Geofences {
  constructor() {
    this.geofences = new Map();
  }

  set(id, geofence) {
    const points = polyline.decode(geofence.polyline);
    const gfInstance = new InNOut.Geofence(points);

    this.geofences.set(id, gfInstance);
  }

  delete(id) {
    this.geofences.delete(id);
  }

  match(point) {
    const response = new Set();

    for (let [index, gfInstance] of this.geofences) {
      if (gfInstance.inside([point.lat, point.lon])) {
        response.add(index);
      }
    }

    return response;
  }
}

module.exports = Geofences;
