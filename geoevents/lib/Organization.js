const Geofences = require('./Geofences.js');
const Geotrigger = require('./Geotrigger.js');
const request = require('request');
const settings = require('../settings.json');
const log = require('./log.js');

class Organization {
  constructor(name, redis) {
    log.info(`[${name}] Organization acknowledge`);

    this.name = name;
    this.redis = redis;
    this.geofences = new Geofences();
    this.geotriggers = new Map();

    this._fetchGeofences();
  }

  _fetchGeofences() {
    const { redis, name, geofences } = this;

    log.info(`[${name}] fetching geofences`);

    redis.smembers(`${name}:geofence:members`, function (err, ids) {
      log.info(`[${name}] ${ids.length} geofences fetched`);

      ids.forEach(geofenceId => {
        redis.hgetall(`${name}:geofence:${geofenceId}:obj`, function (err, geofence) {
          geofences.set(geofence.id, geofence);
        });
      });
    });
  }

  updateGeofence(message) {
    const { geofences, name } = this;
    const geofence = message.data;

    log.info(`[${name}] geofence ${message.event}`);

    switch (message.event) {
      case 'create':
      case 'update':
        geofences.set(geofence.id, geofence)
        break;
      case 'delete':
        geofences.delete(geofence.id, geofence)
        break;
    }
  }

  _getGeotrigger(fleetId, deviceId) {
    const { name, geotriggers } = this;

    let geotrigger = this.geotriggers.get(deviceId);
    if (geotrigger === undefined) {
      log.info(`[${name}] device acknowledge ${deviceId}`);

      geotrigger = new Geotrigger();

      geotrigger.on('onEnter', event => {
        log.info(`[${name}] Device ${deviceId} entered geofence ${event.id}`);
        this._publishEvent('geofence-enter', fleetId, deviceId, event.id, event.last_pos);
      });

      geotrigger.on('onLeave', event => {
        log.info(`[${name}] Device ${deviceId} exited geofence ${event.id}`);
        this._publishEvent('geofence-leave', fleetId, deviceId, event.id, event.last_pos);
      });

      geotriggers.set(deviceId, geotrigger)
    }

    return geotrigger;
  }

  _publishEvent(type, fleetId, deviceId, geofenceId, last_pos) {
    const { redis, name } = this;

    redis.hgetall(`${name}:geofence:${geofenceId}:obj`, function (err, geofence) {
      redis.hgetall(`${name}:device:${deviceId}:obj`, function (err, device) {
        device.last_pos = last_pos;

        const message = {
          event: type,
          data: {
            org_name: name,
            device,
            geofence,
          },
        };

        redis.publish(`${name}:fleet:${fleetId}`, JSON.stringify(message));
      });
    });
  }

  newPosition(fleetId, device) {
    const { last_pos, id } = device;

    const geotrigger = this._getGeotrigger(fleetId, id, last_pos);
    geotrigger.pulse(this.geofences.match(last_pos), last_pos);
  }
}

module.exports = Organization;
