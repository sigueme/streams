const assert = require('assert');
const Geotrigger = require('../Geotrigger.js');


describe('geotrigger', function () {
  it('should emit onEnter message', function (done) {
    const geotrigger = new Geotrigger();

    geotrigger.on('onEnter', function (event) {
      if (event.id === 'a') {
        assert(event.pulse, 2);
      }

      if (event.id === 'b') {
        assert(event.pulse, 5);
        done();
      }
    });

    geotrigger.pulse(new Set([]))
    geotrigger.pulse(new Set(['a']))
    geotrigger.pulse(new Set(['a', 'b']))
    geotrigger.pulse(new Set(['a']))
    geotrigger.pulse(new Set(['a', 'b']))
    geotrigger.pulse(new Set(['a', 'b']))
    geotrigger.pulse(new Set(['a', 'b']))
    geotrigger.pulse(new Set(['a', 'b']))
    geotrigger.pulse(new Set(['a', 'b']))
  })

  it('should emit onLeave message', function (done) {
    const geotrigger = new Geotrigger();

    geotrigger.on('onLeave', function (event) {
      if (event.id === 'a') {
        assert(event.pulse, 7);
        done();
      }

      if (event.id === 'b') {
        assert(event.pulse, 4);
      }
    });

    geotrigger.pulse(new Set(['a', 'b']))
    geotrigger.pulse(new Set(['a']))
    geotrigger.pulse(new Set(['a', 'b']))
    geotrigger.pulse(new Set(['a']))
    geotrigger.pulse(new Set(['a']))
    geotrigger.pulse(new Set(['a']))
    geotrigger.pulse(new Set([]))
    geotrigger.pulse(new Set([]))
    geotrigger.pulse(new Set([]))
    geotrigger.pulse(new Set([]))
    geotrigger.pulse(new Set([]))
  })
});
