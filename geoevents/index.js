const settings = require('./settings.json');
const Organization = require('./lib/Organization.js');
const log = require('./lib/log.js');
const redis = require('redis').createClient(settings.redis);

redis.on('error', function (err) {
  log.error('redis conection error');
});

orgs = new Map();

const pub = redis.duplicate();

pub.on('pmessage', function (match, channel, message) {
  const colonIndex = channel.indexOf(':');
  const orgName = channel.substring(0, colonIndex);

  let scope = orgs.get(orgName);

  if (scope === undefined) {
    scope = new Organization(orgName, redis);
    orgs.set(orgName, scope);
  }

  message = JSON.parse(message);

  const { event, data } = message;

  if (event === 'device-update' && data.last_pos) {
    const fleetId = channel.split(':')[2];

    return scope.newPosition(fleetId, data);
  }

  if (match === '*:geofence') {
    return scope.updateGeofence(message);
  }
});

// subscribe to positions
pub.psubscribe('*:fleet:*');
pub.psubscribe('*:geofence');

log.info('Geoevents started');
