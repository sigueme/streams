# Fleety Websocket

Listens to events in redis pubsub and publishes to a websocket interface so a webapp would be able to listen to them

## Develop

```bash
$ npm install
$ npm start
```
