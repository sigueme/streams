let config = require('./config.json');
let app    = require('http').createServer(handler)
let io     = require('socket.io')(app);
let redis  = require('redis');
let fs     = require('fs');
let log    = require('./lib/log.js');
let SocketBag = require('./lib/socket_bag.js');

// Create socket container
let sb = new SocketBag();

log.info(`Started listening websocket on port ${config.socket.port}`);
app.listen(config.socket.port);

function handler (req, res) {
  fs.readFile(__dirname + '/public/index.html', function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

// redis client for authentication
let redisAuth = redis.createClient(config.redis);

// redis client for subscription
let redisSub = redis.createClient(config.redis);

redisSub.on('pmessage', function (pattern, channel, message) {
  let parsedMessage = {};

  try {
    parsedMessage = JSON.parse(message);
  } catch (e) {
    return log.warning(`Received non-json data from channel '${channel}': ${message}`);
  }

  for (let socket of sb.subscribers(channel)) {
    socket.emit('message', Object.assign({
      channel: channel,
    }, parsedMessage));
  }
});

redisSub.psubscribe('*');

// Authentication
require('socketio-auth')(io, {
  authenticate: function (socket, data, callback) {
    // get credentials sent by the client 
    let api_key = data.api_key;
    let org = data.org;
    let requested_channels = data.channels || [];

    log.info(`Auth request for socket ${socket.id}`);

    if (!api_key) {
      return callback(new Error('api key not given'));
    }

    redisAuth.hget('organization:index_subdomain', org, function (err, org_id) {
      if (err || !org_id) {
        return callback(new Error('Unknown organization'));
      }

      redisAuth.hget('user:index_api_key', api_key, function (err, user_id) {
        if (err || !user_id) {
          return callback(new Error('User not found'));
        }

        redisAuth.sismember(`organization:${org_id}:srel_users`, user_id, function (err, is_member) {
          if (err || !is_member) {
            return callback(new Error('User is not member of organization'));
          }

          return callback(null, true);
        });
      });
    });
  },

  postAuthenticate: function (socket, data) {
    log.info(`Authenticated socket ${socket.id}`);

    // TODO filter channels by permission
    let requested_channels = (data.channels || []).filter(c => true);

    sb.register(socket, requested_channels);

    socket.on('disconnect', (reason) => {
      log.info(`Disconnected socket ${socket.id} with reason '${reason}'`);

      sb.remove(socket);
    });
  },
});
