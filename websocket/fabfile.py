from __future__ import with_statement
from fabric.api import run, cd, env, local
from fabric.context_managers import prefix
from fabric.operations import prompt
import os

HOME = '/home/fleety'
WEBAPPS_ROOT = os.path.join(HOME, 'webapps')
DEVEL_WEBAPPS_ROOT = os.path.join(HOME, 'webapps_devel')

env.hosts = ['getfleety.com']
env.user  = 'fleety'

branch = local('git branch | grep -E "^\*"', capture=True).split()[1]

if branch == 'master':
    PROJECT_PATH = os.path.join(WEBAPPS_ROOT, 'streams.getfleety.com')
    SERVICE = 'fleety-websocket'
else:
    PROJECT_PATH = os.path.join(DEVEL_WEBAPPS_ROOT, 'streams.getfleety.com')
    SERVICE = 'fleety-websocket-devel'

def deploy():
    with cd(PROJECT_PATH):
        run('git pull')

        with cd(os.path.join(PROJECT_PATH, 'websocket')):
            run('npm install --no-package-lock')

    if prompt('Restart server? [y/N]').lower().startswith('y'):
        run('systemctl --user restart {}'.format(SERVICE))
