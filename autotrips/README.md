# fleety-streams/autotrips

## Config
```bash
$ cp settings.sample.json settings.json
$ $EDITOR settings.json
```

## Develop

```bash
$ npm install
$ npm start
```
