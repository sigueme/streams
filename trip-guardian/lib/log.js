module.exports = require('js-logging').console({
  format: "[${title}] ${message} ${file}:${line}",
});
