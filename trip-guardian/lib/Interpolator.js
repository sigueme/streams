const polyline = require('@mapbox/polyline');
const haversine = require('haversine');

module.exports = function Interpolator(route, options) {
  const points = polyline.decode(route)
    .map(point => new Object({
      latitude: point[0],
      longitude: point[1],
    }))

  let out = [];
  for (let i=1; i<points.length; ++i) {
    const curr = points[i];
    const last = points[i-1];

    distance = haversine(curr, last);

    if (distance > options.maxDistance) {
      elements = Math.ceil(distance / options.maxDistance);

      dlat = (curr.latitude - last.latitude) / elements;
      dlon = (curr.longitude - last.longitude) / elements;

      for (let i=0; i<elements; ++i) {
        out.push({
          latitude: last.latitude + (dlat*i),
          longitude: last.longitude + (dlon*i)
        });
      }
    } else {
      out.push(last);
    }
  }

  out.push(points[points.length - 1]);

  return out;
};
