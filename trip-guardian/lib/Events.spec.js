const Events = require('./Events');

class Button extends Events {
  click() {
    this.emit('click');
  }
}

describe('Custom event listeners', function() {
  it('must inherit addEventListener, and emit methods', function(done) {
    const button = new Button();

    button.addEventListener('click', function() {
      done();
    });

    button.click();
  });

  it('must allow listeners to be unbinded', function(done) {
    const button = new Button();

    const f = function() {
      done();
    };

    const g = function() {
      throw "this function not be called";
    };

    button.addEventListener('click', f);
    button.addEventListener('click', g);
    button.removeEventListener('click', g);

    button.click();
  });
});
