const polyline = require('@mapbox/polyline');
const Session = require('./Session');

describe('Session', function () {
  it('should proxy device positions to guardians', function () {
    const session = new Session();

    let t1 = {
      id: 1,
      status: 'ONGOING',
      device_id: 'abc',
    };

    let t2 = {
      id: 2,
      status: 'ONGOING',
      device_id: 'cba',
    };

    session.sync(t1);
    session.sync(t2);

    expect(session._devices.get('abc').has(1)).toEqual(true);
    expect(session._devices.get('abc').has(2)).toEqual(false);

    t2.device_id = 'abc';
    session.sync(t2);

    expect(session._devices.get('abc').has(1)).toEqual(true);
    expect(session._devices.get('abc').has(2)).toEqual(true);

    t1.device_id = 'cba';
    session.sync(t1);

    expect(session._devices.get('abc').has(1)).toEqual(false);
    expect(session._devices.get('abc').has(2)).toEqual(true);
  });

  it('should stop the guardian when trip is finished', function () {
    const session = new Session();

    let t1 = {
      id: 1,
      status: 'ONGOING',
      device_id: 'abc',
    };

    session.sync(t1);

    expect(session._devices.get('abc').has(1)).toEqual(true);

    t1.status = 'FINISHED';
    session.sync(t1);

    expect(session._devices.get('abc').has(1)).toEqual(false);
  });
});
