const polyline = require('@mapbox/polyline');
const Guardian = require('./Guardian');

const testRoute = 'snw|BdhbvRcDzDq@dAcBrCo@|AWn@u@xC}ApJsDzVy@~EEb@{BnNe@rD_AlFuA~IyDdWq@xEm@rCUz@g@pAe@fAeCbFoEjJo@bAWj@qAfCi@|@_A`AmBlAmItD{M`G}DbBoIxDiGnCcElBeFpBaDfA{EzAuM~D_F~AuT`HaCt@gAXiA`@gBh@aBd@_K`D}DlAoDjAsGrBmLnDaGnBaIdCe\\dKaEjAeHzBcBj@sM~DoFbBgRbGwM~D_IdCc@LgH|B{E`BaAd@aD|BaHnFyB~AcBdAeA\\y@RiBVoBHc_@BgOB{CBgEA}FFkCAwGBoGNyDJcQ^cFNmD?m@Eq@Kk@M[MyBkAeAw@WeAA[DYDKRYl@i@\\KZ?\\HZXJZBNAVGV]l@GPgA|@GFUP[XgKpIwJhIsF~EuBbBy@p@iRvOaK~HsJ|HaO|LcFbEmErD{@z@y@jA{@dB_GhN]dAo@fC{CpPs@nEaAvEiK|k@sBzKiCdMSpAoD|RkDlRoAhHkAdH_AxFgAhG{EvW{@bDi@nAaFrJs@tBk@`CY`C_BnSaBtSmEti@e@|HmAjQu@hJIrB?lCJfBVdCdAhHvBhNd@tDb@nC|@bGhAlIj@`Dn@dEtAdLFdAEzAO~CkA~G{AjJe@~CWhDAlABhBH|ArAfIh@zCPhBf@pFHtA@vAErA_@lDi@|DgEt[_@~CeAzFiB|IaCvKa@pAwCjIaBvEk@jAo@dA_@`@q@h@gAj@cDdAcBf@gAb@y@b@k@`@e@d@k@v@u@fBEPU|@QdBK~BGlCSrEc@~MI~AGj@_@vBiBfISpAKpA@rAx@fK^lF`A~K\\~BTr@b@~@|AtC`DxFx@nB`GrRJL\\z@HTlEpOl@rBxBbHvF|QjBzGdDjKp@rBLH`@vAjBtGFVBd@G`BYdBa@jCCv@Fb@Vt@NTx@n@rJxGlA|@DNlFtDhFzDvC|BvAbBlAjBz@nBd@xAn@pC`BdJjBpLr@jE`BbKrAxHdAtG`ApFlCpOnBxL`Lxp@dHnb@tEbXpCpPfEtVvD|UpB`M~Gp`@tDpTfAnG~A|Jx@jEb@xAd@jA~@|AV^hAvAfBfB~AdAnAp@zCz@fQnEde@fLlGzA`GbBhHdBjInBvMbDfFtAjBv@pBpAjBhB|@hAl@`AhCpEbQl[tT~`@|Pd[`GtKtRz]X`@dExH';

const testTrip = 'xfbvRwmw|BlF_EpFcDxFeB|JiBnX{DlVmDxTsD|R_D|N{BtRoIzKyF`IcK|F}MbHqOpEeKhEqJ`Vit@t_@ioAbZw~@`Zg_A`R{\\nBsg@h@uo@j@{`@kGcXvIwCfU{X|m@}t@vXiZlf@qOrfAaS`nB{\\zw@aRl~AyL~|@hG|`AnC~|@~Al|@yMtf@aU|WyWfFyGlTqZ|WsZrl@qRneAi\\tA^z@fIXpFaD~E}HfDr@`HmA~D_FWYnDyCOoB`AeBvBh@fB{@~AeB~@aB@kC@aEnAqEfFoHzIkBhFaHzGuGxJWzAbJdFdP`HbPzEbNrDvKzD|LpDvVxH|PzExN_AnOtS`NxQlZ`J~f@~Hvc@`Ixd@~Hlj@dJbaApP|aA|O`w@`Mt[xN`Kr_@lIx_@nIr[hKb`@n\\`U~f@bXvg@rXtb@hVhSjL'

describe('Guardian', function () {
  it('should find distance from point to route', function () {
    const guardian = new Guardian({});
    guardian.route = testRoute;

    const nearest = guardian._nearestInRoute({
      latitude: 20.5987634,
      longitude: -103.3929539
    });

    expect(nearest.routeIndex).toEqual(0);
    expect(Math.abs(nearest.distance - 1)).toBeLessThan(0.1);
  });

  it('should detect when trip is offroute', function(done) {
    const frame = new Date(2017, 1, 1, 9).getTime();

    const trip = polyline.decode(testTrip)
      .map((point, ix) => new Object({
        latitude: point[0],
        longitude: point[1],
        time: new Date(frame + ix * 60000),
      }));

    const guardian = new Guardian({
      offrouteDistance: 1,
      frecuency: 3600
    });
    guardian.route = testRoute;

    guardian.addEventListener('offroute', function (data) {
      done();
    });

    trip.forEach(point => guardian.acknowledge(point));
  });

  it('should work without known route', function () {
    const guardian = new Guardian({});

    const nearest = guardian._nearestInRoute({
      latitude: 20.5987634,
      longitude: -103.3929539
    });

    expect(nearest).toEqual(null);
  });

  it('should work when vertices are too separated', function () {
    const route = 'qbdyBpgdfRohg@b|wFqtcEepwDnh`CuucG';
    const trip = 'qbdyBpgdfR{`G~wp@}`G`xp@{`G~wp@}`G`xp@{`G~wp@uif@a|c@wif@_|c@uif@a|c@wif@_|c@uif@a|c@ppTmmj@rpTmmj@ppTmmj@ppTomj@rpTmmj@ppTmmj@'

    const frame = new Date(2017, 1, 1, 9).getTime();
    const guardian = new Guardian({});
    guardian.route = route;

    guardian.addEventListener('offroute', function (data) {
      throw 'this should not be called';
    });

    polyline.decode(trip)
      .map((point, ix) => new Object({
        latitude: point[0],
        longitude: point[1],
        time: new Date(frame + ix * 60000),
      }))
      .forEach(point => {
        guardian.acknowledge(point);
      });
  });

  it('should sort points in a lattice', function () {
    const guardian = new Guardian({});

    expect(guardian._lattice({
      latitude: 1,
      longitude: 1
    }, 1)).toEqual([111, 111]);

    expect(guardian._lattice({
      latitude: 10,
      longitude: 10
    }, 1)).toEqual([1112, 1095]);

    expect(guardian._lattice({
      latitude: 10,
      longitude: 0
    }, 1)).toEqual([1112, 0]);

    expect(guardian._lattice({
      latitude: 41.865882,
      longitude: -87.62316
    }, 1)).toEqual([4655, 6902]);
  });

  it('should detect stops', function (done) {
    const frame = new Date(2017, 1, 1, 9).getTime();
    const trip = 'sjn~Fn`yuOmDDgCB}A@eA@eC?cCAsA@_BDsAA}A@sA@{@?_AAk@@y@Dq@As@?g@Q?C@??B@CA@A?B@?AAAAB@???@A??AAA@BAC?@B@??EAB@@QRo@@g@?g@@;'

    const guardian = new Guardian({
      stopRadius: 0.05,
      stopMinTime: 10
    });

    guardian.addEventListener('stop', function (data) {
      expect(Math.abs(data.stop_time - 600000)).toBeLessThan(1000);
      done();
    });

    const guardian2 = new Guardian({
      stopRadius: 0.05,
      stopMinTime: 30
    });

    guardian2.addEventListener('stop', function (data) {
      throw 'this should not be exceduted';
    });

    polyline.decode(trip)
      .map((point, ix) => new Object({
        latitude: point[0],
        longitude: point[1],
        time: new Date(frame + ix*60000),
      }))
      .forEach(point => {
        guardian.acknowledge(point)
        guardian2.acknowledge(point)
      });
  });
});
