const redis = require('redis');

const Guardian = require('./Guardian');
const Events = require('./Events');
const settings = require('../settings.json');
const log = require('./log.js');

class Guardians extends Events {
  constructor() {
    super();

    this._devices = new Map();
    this._trips = new Map();
    this._guards = new Map();
    this._redis = redis.createClient(settings.redis);
  }

  _attach(trip_id, device_id) {
    if (this._devices.has(device_id)) {
      this._devices.get(device_id).add(trip_id);
    } else {
      this._devices.set(device_id, new Set([trip_id]));
    }
  }

  _detach(trip_id, device_id) {
    const ref = this._devices.get(device_id);

    if (ref === undefined) {
      return;
    }

    ref.delete(trip_id);
  }

  _offroute(event) {
    let trip = this._trips.get(event.trip_id);
    let org_name = trip._type.split(':')[0];

    this._redis.hgetall(`${org_name}:device:${trip.device_id}:obj`, (err, device) => {
      device.last_pos = {
        lat: event.position.latitude,
        lon: event.position.longitude,
      };

      this.emit('offroute', {
        type: 'tripOffroute',
        trip,
        device,
        route: event.route,
        time: event.time.toISOString().slice(0, 19)+'Z',
        distance: event.distance,
        org_name: trip._type.split(':')[0],
      });
    });
  }

  _stop(event) {
    let trip = this._trips.get(event.trip_id);
    let org_name = trip._type.split(':')[0];

    this._redis.hgetall(`${org_name}:device:${trip.device_id}:obj`, (err, device) => {
      device.last_pos = {
        lat: event.position.latitude,
        lon: event.position.longitude,
      };

      this.emit('stop', {
        type: 'tripStop',
        trip,
        device,
        org_name,
        arrived: event.arrived.toISOString().slice(0, 19)+'Z',
        stop_time: event.stop_time,
        time: event.time.toISOString().slice(0, 19)+'Z',
      });
    });
  }

  insert(trip) {
    if (trip.status !== 'ONGOING') {
      return 0;
    }

    this._trips.set(trip.id, Object.assign({}, trip));
    this._attach(trip.id, trip.device_id);

    const guardian = new Guardian(settings.guardian, trip.id);

    guardian.addEventListener('offroute', this._offroute.bind(this));
    guardian.addEventListener('stop', this._stop.bind(this));

    this._guards.set(trip.id, guardian);

    if (trip.route_id) {
      log.debug(`[${trip.org_subdomain}] Inserted trip ${trip.id} with route ${trip.route_id}`);
      const route_fqn = trip.org_subdomain + ':route:' + trip.route_id + ':obj';

      this._redis.hget(route_fqn, 'polyline', function (err, route) {
        guardian.route = route;
      });
    }

    if (trip.notify_stop !== null) {
      guardian.options.stopMinTime = trip.notify_stop;
    }
  }

  sync(trip) {
    const prev = this._trips.get(trip.id);
    const guardian = this._guards.get(trip.id);

    if (!prev) {
      return this.insert(trip);
    }

    let changed = false;

    // route changed
    if (prev.route_id !== trip.route_id) {
      log.debug(`[${trip.org_subdomain}] Updated trip ${trip.id} with new route ${trip.route_id}`);
      const route_fqn = trip.org_subdomain + ':route:' + trip.route_id + ':obj';

      this._redis.hget(route_fqn, 'polyline', function (err, route) {
        guardian.route = route;
      });

      changed = true;
    }

    // device changed
    if (prev.device_id !== trip.device_id) {
      this._detach(trip.id, prev.device_id);
      this._attach(trip.id, trip.device_id);

      changed = true;
    }

    // notify_stop changed
    if (prev.notify_stop !== trip.notify_stop) {
      if (trip.notify_stop !== null) {
        guardian.options.stopMinTime = trip.notify_stop;
      } else {
        guardian.options.stopMinTime = 1e9;
      }
    }

    // Save changes
    if (changed) {
      this._trips.set(trip.id, trip);
    }

    // status changed
    if (trip.status === 'FINISHED') {
      this.delete(trip);
    }
  }

  delete(trip) {
    this._detach(trip.id, trip.device_id);
    this._trips.delete(trip.id);
  }

  acknowledge(device_id, point) {
    const trips = this._devices.get(device_id);

    if (!trips) {
      return;
    }

    trips.forEach(trip_id => {
      const guard = this._guards.get(trip_id);

      if (guard) {
        guard.acknowledge(point);
      }
    });
  }
}

module.exports = Guardians;
