const haversine = require('haversine');
const polyline = require('@mapbox/polyline');
const Interpolator = require('./Interpolator');

describe('Interpolator', function () {
  it('should output coolinear points', function () {
    const route = Interpolator('a}b_CbdnzRzmn@aheB', { maxDistance: 1 });

    const A = route[1];
    const B = route[2];
    const C = route[3];

    const AB = haversine(A, B);
    const AC = haversine(A, C);
    const BC = haversine(B, C);

    expect(Math.abs((AB + BC) - AC)).toBeLessThan(0.1);
  });

  it('Initial route should be a subset of the interpolated one', function () {
    const route = 'qbdyBpgdfRohg@b|wFqtcEepwDdfoBaxdG';
    const points = polyline.decode(route)
      .map(point => new Object({
        latitude: point[0],
        longitude: point[1],
      }))

    const interpolated = Interpolator(route, {
      maxDistance: 1
    });

    let j = 0;
    for (let i=0; i<points.length; ++i) {
      while (haversine(points[i], interpolated[j]) > 1e-6) {
        ++j;
      }

      expect(j).toBeLessThan(interpolated.length);
    }
  });

  it('should respect the maxDistance rule', function () {
    const route = 'qbdyBpgdfRohg@b|wFqtcEepwDdfoBaxdG';
    const points = Interpolator(route, { maxDistance: 10 });

    for (let i=1; i<points.length; ++i) {
      let distance = haversine(points[i], points[i-1]);
      expect(distance).toBeLessThan(10);
    }
  })
});
