const Interpolator = require('./Interpolator');
const haversine = require('haversine');
const kdt = require('kd.tree');
const Events = require('./Events');

const INF = 2**53;

class Guardian extends Events {
  constructor(options, trip_id) {
    super();

    this.options = Object.assign({
      offrouteDistance: 1, // kms
      frecuency: 15*60, // notification frecuency in seconds
      threshold: 3*60, // gate activation in seconds

      stopRadius: 0.1, // radius to qualify as the same point
      stopMinTime: 1e9 // stop time to trigger notification
    }, options);

    this.center = [0,0];
    this.mem = [INF,INF,INF,INF,INF,INF,INF,INF,INF];
    this.trip_id = trip_id;
    this.route_obj = null;
  }

  set route(route) {
    if (!route) {
      this._tree = kdt.createKdTree([], haversine, ['latitude', 'longitude']);
      this._routeStatus = {
        state: true,
        thresholdInit: null,
        lastChange: undefined,
      };

      return;
    }

    const points = Interpolator(route, {
      maxDistance: this.options.offrouteDistance
    }).map((point, ix) => {
      point._ix = ix;
      return point;
    });

    this._tree = kdt.createKdTree(points, haversine, ['latitude', 'longitude']);
    this._routeStatus = {
      state: true,
      thresholdInit: null,
      lastChange: undefined,
    };
    this.route_obj = route;
  }

  _nearestInRoute(point) {
    if (!this._tree) {
      return null;
    }

    const ans = this._tree.nearest(point, 1);

    if (ans.length == 0) {
      return null;
    }

    const [ nearest, distance ] = ans[0];

    return {
      routeIndex: nearest._ix,
      latitude: nearest.latitude,
      longitude: nearest.longitude,
      distance: distance
    };
  }

  _lattice(point, step) {
    const cx = haversine(point, {
      latitude: point.latitude,
      longitude: 0,
    });

    const cy = haversine(point, {
      latitude: 0,
      longitude: point.longitude,
    });

    return [Math.round(cy/step), Math.round(cx/step)];
  }

  acknowledge(point) {
    const nearest = this._nearestInRoute(point);

    /* Wrong route detector */
    if (nearest !== null) {
      const { state, thresholdInit } = this._routeStatus;
      const signal = nearest.distance < this.options.offrouteDistance;

      if (state !== signal) {
        if (thresholdInit === null) {
          this._routeStatus.thresholdInit = point.time.getTime();
        } else if (point.time.getTime() - thresholdInit >= this.options.threshold * 1000) {
          this._routeStatus.state = signal;
          this._routeStatus.thresholdInit = null;
          this._routeStatus.lastChange = point.time;
        }
      } else {
        this._routeStatus.thresholdInit = null;
      }

      if (this._routeStatus.state === false) {
        if (!this._lastEmit || point.time - this._lastEmit >= this.options.frecuency * 1000) {
          this.emit('offroute', {
            trip_id: this.trip_id,
            position: point,
            time: point.time,
            route: this.route_obj,
            distance: nearest.distance,
          });

          this._lastEmit = point.time;
        }
      }
    }

    /* Stop detection engine  */
    // When did I arrive here?
    let from = point.time.getTime();

    // Calc new mem matrix
    const center = this._lattice(point, this.options.stopRadius);
    const mem = [INF,INF,INF,INF,INF,INF,INF,INF,INF];

    for (let i=-1; i<2; ++i) {
      for (let j=-1; j<2; ++j) {
        const ni = (this.center[0]+i) - (center[0]);
        const nj = (this.center[1]+j) - (center[1]);

        if (Math.abs(ni) < 2 && Math.abs(nj) < 2) {
          const tmp = this.mem[(i+1)*3 + (j+1)];
          mem[(ni+1)*3 + nj+1] = tmp
          from = Math.min(from, tmp);
        }
      }
    }

    mem[3+1] = Math.min(mem[3+1], point.time.getTime());

    // Update mem
    this.center = center;
    this.mem = mem;

    // Should I notify the stop?
    const diff = point.time.getTime() - from;

    if (diff / 60000 >= this.options.stopMinTime) {
      if (this._stopEmit === null) {
        this.emit('stop', {
          arrived: new Date(from),
          stop_time: diff,
          position: point,
          trip_id: this.trip_id,
          route: this.route_obj,
          time: point.time,
        });

        this._stopEmit = true;
      }
    } else {
      this._stopEmit = null;
    }

    return nearest ? nearest.distance : 0;
  }
}

module.exports = Guardian;
