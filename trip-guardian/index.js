const request = require('request');
const { Client } = require('pg');

const log = require('./lib/log.js');
const Session = require('./lib/Session');
const settings = require('./settings.json');

// Data structure
const session = new Session();

// Connect to redis
const redis = require('redis').createClient(settings.redis);

redis.on('error', function (err) {
  log.error('redis conection error');
});

// Fetch current trips
const sql = new Client(settings.postgres);
sql.connect();

sql.query('SELECT * FROM trip WHERE status = \'ONGOING\' AND device_id IS NOT NULL', (err, res) => {
  if (err) {
    throw err;
  }

  res.rows.forEach(trip => {
    trip._type = `${trip.org_subdomain}:trip`;

    log.info(`[${trip.org_subdomain}] Trip ${trip.id} read from database, syncing`);
    session.sync(trip);
  });

  sql.end();
});

// Subscribe to trip updates
const tripPub = redis.duplicate();

tripPub.on('pmessage', function (match, channel, message) {
  message = JSON.parse(message);
  let pieces = channel.split(':');
  let org = pieces[0];

  const { trip } = message.data;

  if (trip === undefined || !trip._type.endsWith(':trip')) {
    return;
  }

  trip.org_subdomain = org;

  switch (message.event) {
    case 'create':
      log.info(`[${org}] Trip ${trip.id} created, syncing`);
      session.sync(trip);
      break;
    case 'trip-started':
      log.info(`[${org}] Trip ${trip.id} started, syncing`);
      session.sync(trip);
      break;
    case 'update':
      log.info(`[${org}] Trip ${trip.id} updated, syncing`);
      session.sync(trip);
      break;
    case 'trip-finished':
      log.info(`[${org}] Trip ${trip.id} finished, syncing`);
      session.delete(trip);
      break;
    case 'delete':
      log.info(`[${org}] Trip ${trip.id} deleted, syncing`);
      session.delete(trip);
      break;
  }
});

tripPub.psubscribe(`*:fleet:*`);

// Subscribe to new positions
const positionPub = redis.duplicate();

positionPub.on('pmessage', function (match, channel, message) {
  message = JSON.parse(message);

  if (message.event === 'device-update' && message.data.last_pos) {
    const { last_pos, id } = message.data;

    session.acknowledge(id, {
      latitude: last_pos.lat,
      longitude: last_pos.lon,
      time: new Date(message.data.last_update),
    });
  }
});

positionPub.psubscribe('*:fleet:*');

// Listen to offroutes
session.addEventListener('offroute', event => {
  const message = {
    event: 'alarm',
    data: event,
  };

  log.info(`[${event.org_name}] trip ${event.trip.id} offroute`);
  redis.publish(`${event.org_name}:fleet:${event.trip.fleet_id}`, JSON.stringify(message));
});

// Listen to stops
session.addEventListener('stop', event => {
  const message = {
    event: 'alarm',
    data: event,
  };

  log.info(`[${event.org_name}] trip ${event.trip.id} stopped`);
  redis.publish(`${event.org_name}:fleet:${event.trip.fleet_id}`, JSON.stringify(message));
});

log.info('Trip guardian started');
